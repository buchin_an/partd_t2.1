package com.company;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    private final static int ID_PRIVATEBANK = 0;
    private final static int ID_PUMB = 1;
    private final static int ID_OSHADBANK = 2;

    static final String[] bankNames = {"PrivateBank", "PUMB", "Oshadbank"};
    static final float[] rates = {24.5f, 25.0f, 23.5f};

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Enter the amount of money that you want to change: ");
        int amount = Integer.parseInt(scan.nextLine());

        System.out.println("Enter the bank's name: ");
        String bankName = scan.nextLine();

        if (bankName.equalsIgnoreCase(bankNames[ID_PRIVATEBANK])) {
            System.out.println(String.format(Locale.US, "You money in USD %.2f", amount * rates[ID_PRIVATEBANK]));
        } else if (bankName.equalsIgnoreCase(bankNames[ID_PUMB])) {
            System.out.println(String.format(Locale.US, "You money in USD %.2f", amount * rates[ID_PUMB]));
        } else if (bankName.equalsIgnoreCase(bankNames[ID_OSHADBANK])) {
            System.out.println(String.format(Locale.US, "You money in USD %.2f", amount * rates[ID_OSHADBANK]));
        } else {
            System.out.println("Unknown bank");
        }

    }
}
